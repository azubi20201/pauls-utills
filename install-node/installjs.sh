#!/bin/sh
if [ -n "$HOME" ]; then
    mkdir $HOME/node
    mkdir $HOME/node/local

FILENAME=$(wget -q "https://nodejs.org/dist/latest" -O - | cut -d '"' -f 2 | cut -d '/' -f 1 | grep "linux-x64.tar.xz")
wget -c "https://nodejs.org/dist/latest/$FILENAME"
tar -xf $FILENAME -C $HOME/node/local/ --strip-components=1
zenity --text-info --checkbox="Accept" --filename="$HOME/node/local/LICENSE" --width="1000" --height="600" --title="Liznse"
if [ ! $? -eq 0 ]
then
    rm -rf $FILENAME $HOME/node
else
    rm $FILENAME $HOME/node/local/README.md $HOME/node/local/CHANGELOG.md $HOME/node/local/LICENSE
    grep -qxF 'export PATH="$HOME/node/local/bin:$PATH"' ~/.bashrc || echo "export PATH=\"\$HOME/node/local/bin:\$PATH\"" >> ~/.bashrc
fi
else
  echo "Home is unset"
fi
