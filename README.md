[[_TOC_]]

# Introduction

Linux guides, tweaks and tools.

## [firefox-independent-install](./firefox-independent-install/README.md)

Script ,um den veralteten Firefox in den Standart Repos zu umgehen, da dieser sich selbst nicht updaten kann.

## [flathub](./flathub/README.md)

Guide um Flathub so zu konfiguriren, dass ihr den Lokal nutzen könnt um Applikationwn zu installieren.

## [Gnome-Customizing-Guide](./Gnome-Customizing-Guide/README.MD)

Guide, wie ihr euren Deskop eueren Designwünschen anpassen könnt.

## [install-node](./install-node/README.md)

Script um Nodejs zu installieren

## [zsh-installer](./zsh-installer/README.md)

Script to Install zsh and a few Plugins
